﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Thanks for:
 *	https://www.youtube.com/watch?v=ReauId6jFFI&t=939s
 * for the tutorial
 */
public class player_script : MonoBehaviour {


	float speed = 30;
	float rotSpeed = 80;
    float rot = 0f;
    float gravity = 8;

    Vector3 moveDir = Vector3.zero;
    CharacterController controller;
    Animator animator;

    // Use this for initialization
    void Start () {
		controller = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        movement();
    }

    void movement(){
        if (controller.isGrounded)
        {
            if (Input.GetKey(KeyCode.W))
            {
                animator.SetInteger("condition", 1);
                moveDir = new Vector3(0, 0, 1);
                moveDir *= speed;
                moveDir = transform.TransformDirection(moveDir);
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                animator.SetInteger("condition", 0);
                moveDir = Vector3.zero;
            }
        }
        rot += Input.GetAxis("Horizontal") * Time.deltaTime * rotSpeed;
        transform.eulerAngles = new Vector3(0, rot, 0);

        moveDir.y -= gravity * Time.deltaTime;
        controller.Move(moveDir * Time.deltaTime);
    }
}
